# JoomlaDay UK 2014

## Developing Joomla modules for beginners

Example modules:
https://bitbucket.org/hwdmediashare/jd14uk/src

Joomla Documentation on manifest files: 
http://docs.joomla.org/Manifest_files

Joomla Documentation on standard form field types:
http://docs.joomla.org/Standard_form_field_types

Free module creation tool:
http://joomlabuzz.com/architect

Get in touch with Dave Horsfall:
https://twitter.com/Dave_Horsfall
