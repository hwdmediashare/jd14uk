<?php
/**
 * @package     Joomla.site
 * @subpackage  Module.example4
 *
 * @copyright   (C) 2014 Dave Horsfall
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

class modExample4
{
	public function __construct($module, $params)
	{
                // Get data.
                $this->module = $module;                
                $this->params = $params;                
                
                // Add assets to the head tag.
                $this->addHead();
	}

	public function addHead()
	{
		$doc = JFactory::getDocument();
		$doc->addStyleSheet(JURI::root() . 'modules/mod_example4/css/default.css');
	}
}
