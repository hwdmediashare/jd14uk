<?php
/**
 * @package     Joomla.site
 * @subpackage  Module.example4
 *
 * @copyright   (C) 2014 Dave Horsfall
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

JLoader::register('modExample4', dirname(__FILE__).'/helper.php');

$helper = new modExample4($module, $params);

require JModuleHelper::getLayoutPath('mod_example4', $params->get('layout', 'default'));