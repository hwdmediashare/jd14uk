<?php
/**
 * @package     Joomla.site
 * @subpackage  Module.example4
 *
 * @copyright   (C) 2014 Dave Horsfall
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;
?>
<div class="jd14uk">
    
    <a target="_blank" href="http://www.joomladay.co.uk/">
        <img width="250" height="250" src="<?php echo JURI::root(); ?>modules/mod_example4/images/JD_UK_Speaker_TheDandy.png">
    </a>
    
    <?php if ($params->get('like') == 1): ?>
        <h2><?php echo JText::_('MOD_EXAMPLE4_DOES_LIKE_JOOMLA_HEAD'); ?></h2>
        <p><?php echo JText::_('MOD_EXAMPLE4_DOES_LIKE_JOOMLA_TEXT'); ?></p>
    <?php else: ?>
        <h2><?php echo JText::_('MOD_EXAMPLE4_DOES_NOT_LIKE_HEAD'); ?></h2>
        <p><?php echo JText::_('MOD_EXAMPLE4_DOES_NOT_LIKE_TEXT'); ?></p>
        <img src="http://media.247sports.com/Uploads/Assets/122/187/1187122.gif" />
    <?php endif; ?> 
      
</div>
